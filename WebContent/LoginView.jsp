<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<link rel="stylesheet" type="text/css" href="css/main.css" />
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">

<title>Researchers</title>
</head>
<body>
<c:if test="${not empty user }">
	<p> You are authenticated as {user.id}</p>
</c:if>
<c:if test="${empty user }">
	<a href="LoginServlet">Login</a>
</c:if>
<p style="color: red;">${message}</p>
<h3>
	<a href="ResearchersListServlet">Researchers list</a>
</h3>
<hr>

</body>
</html>