<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<%@ include file = "Header.jsp"%>
<title>Publication info</title>
</head>
<body>
<table>
<tr>
	<td>Publication id</td>
	<td>${pi.id}</td>
</tr>
<tr>
	<td>Title</td>
	<td>${pi.title}</td>
</tr>
<tr>
	<td>Name</td>
	<td>${pi.publicationName}</td>
</tr>
<tr>
	<td>Date</td>
	<td>${pi.publicationDate}</td>
</tr>
<tr>
	<td>Authors id</td>
	<td>${pi.authors}</td>
</tr>
<tr>
	<td>Number of cites</td>
	<td>${pi.citeCount}</td>
</tr>

</table>

</body>
</html>