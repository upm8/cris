package es.upm.dit.apsv.cris.servlets;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

import javax.json.JsonObject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.jsonp.JsonProcessingFeature;

import es.upm.dit.apsv.cris.model.Publication;
import es.upm.dit.apsv.cris.model.Researcher;

/**
 * Servlet implementation class CreateResearcherServlet
 */
@WebServlet("/CreateResearcherServlet")
public class CreateResearcherServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
 

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	/*protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	
	}*/

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
			//Request parameters: Researcher data from form
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String lastname = request.getParameter("lastname");
		String email = request.getParameter("email");
		
				
			//Control checks: Check logged as Admin
		Researcher r = (Researcher) request.getSession().getAttribute("user");
		if (r != null && r.getId().contentEquals("root")) {
			//Persistence operations: create (researcher)
			//Response: sendRedirect: ViewResearcher
			
			Researcher rnew = new Researcher();
			rnew.setId(id);
			rnew.setName(name);
			rnew.setLastname(lastname);
			rnew.setEmail(email);
			
			//Comprobar que no exista un usuario con la misma ID.
			
			Client client = ClientBuilder.newClient(new ClientConfig());
			//client.target(URLHelper.getInstance().getCrisURL()+"/rest/Researchers/").request().post
			request.setAttribute("ri", rnew);
			//response.sendRedirect(request.getContextPath() + "/AdminServlet" + "?id="
			//        + rnew.getId());
			response.sendRedirect(request.getContextPath() + "/AdminServlet");
			//getServletContext().getRequestDispatcher("/AdminServlet.jsp").forward(request, response);
			//getServletContext().getRequestDispatcher("/ResearcherView.jsp").forward(request, response);
		}
		else
			response.sendRedirect(request.getContextPath() + "/LoginView");	
		
			//Persistence operations: create (researcher)
			//Response: sendRedirect: ViewResearcher
			//Model info: researcher id
		
		
		
		
	}

}
