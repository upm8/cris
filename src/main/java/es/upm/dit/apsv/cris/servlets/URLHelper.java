package es.upm.dit.apsv.cris.servlets;


public class URLHelper {
	
	private static URLHelper uh;
	
	public static URLHelper getInstance() {
		if( null == uh ) uh = new URLHelper();
		return uh;
	}
	
	private String restUrl;
	
	public URLHelper() {
		String enValue = System.getenv("CRISSERVICE_SRV_SERVICE_HOST");
		if(enValue == null) //not in Kubernetes
			restUrl = "http://localhost:8080/CRISSERVICE";
		else //in KB, DNS service resolution in Kubernetes
			restUrl = "http://crisservice-srv/CRISSERVICE";
	}
	
	public String getCrisURL() {
		return restUrl;
	}

}
