package es.upm.dit.apsv.cris.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

import org.glassfish.jersey.client.ClientConfig;

import es.upm.dit.apsv.cris.model.Publication;
import es.upm.dit.apsv.cris.model.Researcher;

/**
 * Servlet implementation class CreatePublicationServlet
 */
@WebServlet("/CreatePublicationServlet")
public class CreatePublicationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreatePublicationServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
    @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	//Request parameters: Researcher data from form
		String id = request.getParameter("id");
		String title = request.getParameter("title");
		String pName = request.getParameter("publicationName");
		String date = request.getParameter("publicationDate");
		
			
			Publication pnew = new Publication();
			pnew.setId(id);
			pnew.setTitle(title);
			pnew.setPublicationName(pName);
			pnew.setPublicationDate(date);
			
			
			Client client = ClientBuilder.newClient(new ClientConfig());
			request.setAttribute("pi", pnew);
			response.sendRedirect(request.getContextPath() + "/AdminServlet" + "?id="
			        + pnew.getId());
		

		
	}

}
